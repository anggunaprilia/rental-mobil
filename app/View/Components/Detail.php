<?php

namespace App\View\Components;

use Closure;
use Illuminate\View\Component;
use Illuminate\Contracts\View\View;

class Detail extends Component
{
    /**
     * Create a new component instance.
     */
    public $sewa;
    public function __construct($sewa)
    {
        $this->sewa = $sewa;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.detail');
    }
}
