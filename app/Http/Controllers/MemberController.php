<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Mobil;
use App\Models\Transaksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;


class MemberController extends Controller
{
    public function index(){
        $mobil = Mobil::all();
        return view('member.index', compact('mobil'));
    }

    public function sewa(){
        $mobil = Mobil::all();
        $user = User::where('role', '=', 2)->get();
        $sewa = Transaksi::get();
        return view('member.sewa', compact('user', 'mobil', 'sewa'));
    }

    public function proses(Request $request){
        $request -> validate([
            'user_id'=> 'required',
            'mobil_id'=> 'required',
            'durasi_sewa'=> 'required'
        ]);

        $transaksi = new Transaksi();
        $transaksi -> user_id = $request ->user_id;
        $transaksi -> mobil_id = $request ->mobil_id;
        $transaksi -> durasi_sewa = $request ->durasi_sewa;
        $transaksi->save();
        return redirect("/member/sewa");
    }

    public function detail(){
        // dd(Auth::user());
        $sewa = Transaksi::where('id', Auth::user()->id)->get();
        return view('member.detail', compact('sewa'));
    }

    public function pembayaran($id){
        $sewa = Transaksi::find($id);
        return view('member.pembayaran', compact('sewa'));
    }

    public function pembayaranproses(Request $request, $id){
        $sewa = Transaksi::find($id);
        if($request->has('pembayaran')){
            $path = 'img/';
            File::delete($path. $sewa->pembayaran);
            $fileName = time().'.'.$request->pembayaran->extension();
            $request->pembayaran->move(public_path('img'), $fileName);
            $sewa->pembayaran = $fileName;
            $sewa->update();
        }
        return redirect('member/detail');
    }
}
