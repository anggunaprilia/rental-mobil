<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Mobil;
use App\Models\Transaksi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;

class SewaController extends Controller
{
    public function sewa(){
        $user = User::get();
        $mobil = Mobil::get();
        $sewa = Transaksi::get();
        return view('sewa.sewa', compact('user', 'mobil', 'sewa'));
    }

    public function proses(Request $request){
        $request -> validate([
            'user_id'=> 'required',
            'mobil_id'=> 'required',
            'durasi_sewa'=> 'required'
        ]);

        $transaksi = new Transaksi();
        $transaksi -> user_id = $request ->user_id;
        $transaksi -> mobil_id = $request ->mobil_id;
        $transaksi -> durasi_sewa = $request ->durasi_sewa;
        $transaksi->save();
        return redirect("/sewa");
    }

    public function detail(){
        $sewa = Transaksi::with(['Pengguna', 'Mobil'])->get();
        return view('sewa.detail', compact('sewa'));
    }

    public function pembayaran($id){
        $sewa = Transaksi::find($id);
        return view('sewa.pembayaran', compact('sewa'));
    }

    public function pembayaranproses(Request $request, $id){
        $sewa = Transaksi::find($id);
        if($request->has('pembayaran')){
            $path = 'img/';
            File::delete($path. $sewa->pembayaran);
            $fileName = time().'.'.$request->pembayaran->extension();
            $request->pembayaran->move(public_path('img'), $fileName);
            $sewa->pembayaran = $fileName;
            $sewa->update();
        }
        return redirect('sewa.detail');
    }

    public function status($id){
        $sewa = Transaksi::find($id);
        return view('sewa.status', compact('sewa'));
    }

    public function statusproses(Request $request, $id){
        $sewa = Transaksi::find($id);
        $sewa->update($request->all());
        $sewa->update();

        return redirect('/detail');
    }
}
