<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Mobil;
use App\Models\Kategori;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(){
        $hitungmobil = Mobil::count();
        $hitungkategori = Kategori::count();
        $hitunguser = User::count();
        return view('dashboard.index', compact('hitungmobil', 'hitungkategori', 'hitunguser'));
    }
}
