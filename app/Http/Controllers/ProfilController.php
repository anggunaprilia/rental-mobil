<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProfilController extends Controller
{
    public function create()
    {
        return view('profil.tambah');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'bio' => 'required',
            'alamat' => 'required'
        ]);

        DB::table('profil')->insert([
            'nama' => $request['nama'],
            'bio' => $request['bio'],
            'alamat' => $request['alamat']
        ]);

        return redirect('/profil');
    }
    public function index()
    {
        $profil = DB::table('profil')->get();

        return view('profil.tampil', ['profil' => $profil]);
    }
    public function show($id)
    {
        $profil = DB::table('profil')->where('id', $id)->first();

        return view('profil.detail', ['profil' => $profil]);
    }
    public function edit($id)
    {
        $profil = DB::table('profil')->where('id', $id)->first();

        return view('profil.edit', ['profil' => $profil]);
    }
    public function destroy($id)
    {
        DB::table('profil')->where('id', $id)->delete();

        return redirect('/profil');
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'bio' => 'required',
            'alamat' => 'required'
        ]);

        DB::table('profil')
            ->where('id', $id)
            ->update(
                [
                    'nama' => $request->nama,
                    'bio' => $request->bio,
                    'alamat' => $request->alamat
                ],
            );

        return redirect('/profil');
    }
}
