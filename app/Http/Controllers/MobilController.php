<?php

namespace App\Http\Controllers;


use App\Models\Mobil;
use App\Models\Kategori;
//use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use PDF;

class MobilController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $mobil = Mobil::get();
        $kategori=Kategori::get();
        return view('mobil.index', ['mobil' => $mobil, 'kategori' => $kategori]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $mobil = Mobil::all();
        $kategori = Kategori::all();
        return view('mobil.create', compact('mobil', 'kategori'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        $request -> validate([
            'nama'=> 'required',
            'harga'=> 'required',
            'jumlah_kursi'=> 'required',
            'kategori_id'=> 'required',
            'gambar' => 'required |image|mimes:jpg,png,jpeg'
        ]);

        $fileName = time().'.'.$request->gambar->extension();
        $request->gambar->move(public_path('img'), $fileName);

        $mobil = new Mobil;
        $mobil -> nama = $request ->nama;
        $mobil -> harga = $request ->harga;
        $mobil -> jumlah_kursi = $request ->jumlah_kursi;
        $mobil -> kategori_id = $request ->kategori_id;
        $mobil -> gambar = $fileName;
        $mobil->save();

        // $mobil = Mobil::create($request->all());
        // if($request->hasFile('gambar')){
        //     $request->file('gambar')->move('img/', $request->file('gambar')->getClientOriginalName());
        //     $mobil->gambar = $request->file('gambar')->getClientOriginalName();
        //     $mobil->save();
        // }
        return redirect("/mobil");
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $mobil = Mobil::find($id);
        $kategori = Kategori::get();
        return view ("mobil.edit", compact('mobil', 'kategori'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        // $request -> validate([
        //     'nama'=> 'required',
        //     'harga'=> 'required',
        //     'jumlah_kursi'=> 'required',
        //     'kategori_id'=> 'required',
        //     'gambar' => 'image|mimes:jpg,png,jpeg'
        // ]);

        $mobil = Mobil::find($id);
        //$mobil->update($request->all());
        if($request->has('gambar')){
            $path = 'img/';
            File::delete($path. $mobil->gambar);
            $fileName = time().'.'.$request->gambar->extension();
            $request->gambar->move(public_path('img'), $fileName);
            $mobil->gambar = $fileName;
            $mobil->update();
        }
        $mobil->nama = $request['nama'];
        $mobil->harga = $request['harga'];
        $mobil->jumlah_kursi = $request['jumlah_kursi'];
        $mobil->kategori_id = $request['kategori_id'];
        $mobil->update();

        return redirect("/mobil");
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $mobil = Mobil::find($id);
        $mobil->delete();
        return redirect("/mobil");
    }

    public function pdf(){
        $mobil = Mobil::all();
        view()->share('mobil', $mobil);
        $pdf = PDF::loadView('mobil.datamobil-pdf');
        return $pdf->download('datamobil.pdf');
    }
}
