<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
  public function register()
  {
    return view('auth.register');
  }

  public function registeruser(Request $request)
  {
    User::create([
      'nama' => $request->nama,
      'email' => $request->email,
      'role' => $request->role,
      'password' => bcrypt($request->password)
    ]);
    return redirect('/login');
  }

  public function login()
  {
    return view('auth.login');
  }

  public function loginproses(Request $request)
  {
    // if(Auth::attempt($request->only('email', 'password'))){
    //     return redirect('/mobil');
    // }
    // return redirect('/login');
    //dd($request->all());
    $request->validate([
      'email' => 'required',
      'password' => 'required'
    ]);

    $kredensial = $request->only('email', 'password');

    if (Auth::attempt($kredensial)) {
      $request->session()->regenerate();
      $user = Auth::user();
      if ($user->role == '1') {
        return redirect('/dashboard');
      } elseif ($user->role == '2') {

        return redirect('/member');
      }
      return redirect('/login');
    }
    return redirect('/login');
  }

  public function logout(Request $request)
  {
    Auth::logout();
    $request->session()->invalidate();
    $request->session()->regenerateToken();
    return redirect('/');
  }
}
