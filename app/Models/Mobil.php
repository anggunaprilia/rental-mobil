<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mobil extends Model
{
    use HasFactory;

    protected $table = "mobil";
    protected $fillable = ['nama', 'harga', 'jumlah_kursi', 'kategori_id'];

    public function Mobil()
    {
        return $this->belongsTo(Kategori::class, 'id', 'id');
    }

}
