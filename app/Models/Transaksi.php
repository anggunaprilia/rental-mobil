<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    use HasFactory;
    protected $table = "transaksi";
    protected $fillable = ['user_id', 'mobil_id', 'durasi_sewa', 'jumlah', 'pembayaran', 'status_pembayaran'];

    public function Pengguna()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function Mobil()
    {
        return $this->belongsTo(Mobil::class, 'mobil_id', 'id');
    }
    public function Durasi()
    {
        return $this->belongsTo(Mobil::class, 'harga', 'harga');
    }
}
