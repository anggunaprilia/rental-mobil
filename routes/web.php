<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MobilController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProfilController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\MemberController;
use App\Http\Controllers\SewaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('Home.index');
// });

Route::get('/', [HomeController::class, 'index']);
Route::get('/about', [HomeController::class, 'about']);

Route::middleware('admin')->group(function () {

    //dashboard
    Route::get('/dashboard', [DashboardController::class, 'index']);

    //CRUD mobil
    Route::resource('/mobil', MobilController::class);
    Route::get('/pdf', [MobilController::class, 'pdf']);
    //Route::get('/mobil/create', [MobilController::class, 'create']);

    //CRUD user
    Route::resource('/user', UserController::class);

    //CRUD profil
    //Form Tambah
    Route::get('/profil/create', [ProfilController::class, 'create']);
    //Kirim + tambah data ke database
    Route::post('/profil', [ProfilController::class, 'store']);

    //Read
    //Tampil semua data
    Route::get('/profil', [ProfilController::class, 'index']);

    //Detail
    Route::get('/profil/{profil_id}', [ProfilController::class, 'show']);

    //Update
    //Form Update
    Route::get('/profil/{profil_id}/edit', [ProfilController::class, 'edit']);

    //Update berdasarkan id
    Route::put('/profil/{profil_id}', [ProfilController::class, 'update']);

    //Delete
    Route::delete('/profil/{profil_id}', [ProfilController::class, 'destroy']);

    //User Controller
    Route::get('/users', [UserController::class, 'index'])->name('user.index');
    Route::get('/users/create', [UserController::class, 'create'])->name('user.create');
    Route::post('/users', [UserController::class, 'store'])->name('user.store');
    Route::get('/users/{user}/edit', [UserController::class, 'edit'])->name('user.edit');
    Route::put('/users/{user}', [UserController::class, 'update'])->name('user.update');
    Route::delete('/users/{user}', [UserController::class, 'destroy'])->name('user.destroy');

<<<<<<< HEAD
    Route::get('/kategoris', [KategoriController::class, 'index'])->name('kategori.index');
Route::get('/kategoris/create', [KategoriController::class, 'create'])->name('kategori.create');
Route::post('/kategoris', [KategoriController::class, 'store'])->name('kategori.store');
Route::get('/kategoris/{kategori}/edit', [KategoriController::class, 'edit'])->name('kategori.edit');
Route::put('/kategoris/{kategori}', [KategoriController::class, 'update'])->name('kategori.update');
Route::delete('/kategoris/{kategori}', [KategoriController::class, 'destroy'])->name('kategori.destroy');

=======
    //Kategori
    Route::get('/kategoris', [UserController::class, 'index'])->name('kategori.index');
    Route::get('/kategoris/create', [UserController::class, 'create'])->name('kategori.create');
    Route::post('/kategoris', [UserController::class, 'store'])->name('kategori.store');
    Route::get('/kategoris/edit', [UserController::class, 'edit'])->name('kategori.edit');
    Route::put('/kategoris', [UserController::class, 'update'])->name('kategori.update');
    Route::delete('/kategoris', [UserController::class, 'destroy'])->name('kategori.destroy');
>>>>>>> f9f8c95544972999653f8eb7a883dba231164177

    //
    Route::get('/sewa', [SewaController::class, 'sewa']);
    Route::post('/sewaproses', [SewaController::class, 'proses']);
    Route::get('/detail', [SewaController::class, 'detail']);
    Route::get('/detail/{id}', [SewaController::class, 'pembayaran']);
    Route::put('/detail/{id}', [SewaController::class, 'pembayaranproses']);
    Route::get('/detail/{id}/status', [SewaController::class, 'status']);
    Route::put('/status/{id}', [SewaController::class, 'statusproses']);
});

//Auth
Route::get('/register', [AuthController::class, 'register']);
Route::post('/registerUser', [AuthController::class, 'registeruser']);
Route::get('/login', [AuthController::class, 'login']);
Route::post('/loginproses', [AuthController::class, 'loginproses']);
Route::get('/logout', [AuthController::class, 'logout']);


Route::middleware('member')->group(function () {

    //member
    Route::get('/member', [MemberController::class, 'index']);
    Route::get('/member/sewa', [MemberController::class, 'sewa']);
    Route::post('/member/sewaproses', [MemberController::class, 'proses']);
    Route::get('/member/detail', [MemberController::class, 'detail']);
    Route::get('/member/detail/{id}', [MemberController::class, 'pembayaran']);
    Route::put('/member/detail/{id}', [MemberController::class, 'pembayaranproses']);
});
