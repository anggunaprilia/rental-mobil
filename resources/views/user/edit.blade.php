

@extends('Layout.master')

@section('judul')
    Edit Pengguna
@endsection

@section('content')
<div class="card-body">
    <h4 class="card-title">Edit Data Pengguna</h4>
    <form action="/users/{{$user->id}}" class="forms-sample" method="POST">
        @csrf
        @method('put')
      <div class="form-group">
        <label for="exampleInputName1">Nama</label>
        <input type="text" name="nama" id="nama" class="form-control" value="{{ $user->nama }}" required>
      </div>
      <div class="form-group">
        <label for="exampleInputName1">Email</label>
        <input type="text" name="email" id="email" class="form-control" value="{{ $user->email }}" required>
      </div>
      <div class="form-group">
        <label for="exampleInputName1">password</label>
        <input type="password" name="password" id="password" class="form-control" value="{{ $user->password }}" required>
      </div>
      <div class="form-group">
        <label for="exampleFormControlSelect2">Role</label>
        <select class="form-control" name="role" value="{{ $user->role }}" id="exampleFormControlSelect2">
            <option value="1">Admin</option>
            <option value="2">Member</option>
        </select>
      </div>
      <button type="submit" class="btn btn-primary mr-2">Submit</button>
      <a href="/kategoris" class="btn btn-light">Cancel</a>
      <script>
        const btn = document.getElementByClassName('btn btn-primary mr-2');
        btn.addEventListener('click', function(){
          Swal.fire('Berhasil Submit')
        });
      </script>
    </form>
  </div>


@endsection