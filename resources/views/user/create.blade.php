

@extends('Layout.master')

@section('judul')
    Tambah Pengguna
@endsection

@section('content')
<div class="card-body">
    <h4 class="card-title">Tambah Data Pengguna</h4>
    <form action="/users" class="forms-sample" method="POST">
        @csrf
      <div class="form-group">
        <label for="exampleInputName1">Nama</label>
        <input type="text" name="nama" id="nama" class="form-control" required>
      </div>
      <div class="form-group">
        <label for="exampleInputName1">Email</label>
        <input type="text" name="email" id="email" class="form-control" required>
      </div>
      <div class="form-group">
        <label for="exampleInputName1">Password</label>
        <input type="password" name="password" id="password" class="form-control" required>
      </div>
      <div class="form-group">
        <label for="exampleFormControlSelect2">Role</label>
        <select class="form-control" name="role" id="exampleFormControlSelect2">
            <option value="1">Admin</option>
            <option value="2">Member</option>
        </select>
    </div>
      <button type="submit" class="btn btn-primary mr-2">Submit</button>
      <a href="/users" class="btn btn-light">Cancel</a>
      <script>
        const btn = document.getElementByClassName('btn btn-primary mr-2');
        btn.addEventListener('click', function(){
          Swal.fire('Berhasil Submit')
        });
      </script>
    </form>
  </div>


@endsection
