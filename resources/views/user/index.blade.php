

@extends('Layout.master')

@section('judul')
    Data User
@endsection

@section('content')

<div class="card-body">
    <h4 class="card-title">Data User</h4>
    <a href="/users/create" class="btn btn-secondary btn-rounded btn-fw">Tambah</a>
    <div class="table-responsive">
      <table class="table table-striped">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nama</th>
            <th scope="col">Email</th>
            <th scope="col">Role</th>
            <th scope="col">Aksi</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($users as $item)
              <tr>
                <td scope="row">{{$loop -> iteration}}</td>
                <td>{{$item->nama}}</td>
                <td>{{$item->email}}</td>
                <td>{{$item->role}}</td>

                <td>

                  <form action="/user/{{$item->id}}" method="POST">

                    @csrf
                    @method('DELETE')
                    <a href="/user/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                <script>
                  const btn = document.getElementClassName('btn btn-primary mr-2');
                  btn.addEventListener('click', function(){
                    Swal.fire('Berhasil Submit')
                  });
                </script>
                </form>

                </td>
              </tr>
              @empty
              <tr>
                  <td>Tidak ada data</td>
              </tr>
              @endforelse
        </tbody>
      </table>
    </div>
  </div>
@endsection