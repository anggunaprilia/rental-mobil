@extends('Layout.master')

@section('judul')
    Data Kategori
@endsection

@section('content')

<div class="card-body">
    <h4 class="card-title">Data Kategori</h4>
    <a href="/kategoris/create" class="btn btn-secondary btn-rounded btn-fw">Tambah</a>
    @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
    <div class="table-responsive my-3">
      <table id="example1" class="table table-striped">
        <thead>
          <tr>
            <th>No</th>
            <th>
              Jenis
            </th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($kategoris as $kategori)
              <tr>
                <td scope="row">{{$loop -> iteration}}</td>
                <td>{{$kategori -> jenis}}</td>
                <td>

                    <a href="{{ route('kategori.edit', $kategori) }}" class="btn btn-sm btn-warning">Edit</a>
                    <form action="{{ route('kategori.destroy', $kategori) }}" method="POST" class="d-inline">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm ('Apakah Anda yakin ingin menghapus kategori ini?')">Hapus</button>
                    </form>
                </td>
              </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
@endsection