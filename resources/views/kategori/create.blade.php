

@extends('Layout.master')

@section('judul')
    Tambah Kategori
@endsection

@section('content')
<div class="card-body">
    <h4 class="card-title">Tambah Data Kategori</h4>
    <form action="{{ route('kategori.store') }}" class="forms-sample" method="POST">
        @csrf
      <div class="form-group">
        <label for="exampleInputName1">Jenis</label>
        <input type="text" name="jenis" id="jenis" class="form-control" required>

      </div>
      <button type="submit" class="btn btn-primary mr-2" id="alert">Submit</button>
      <a href="/kategoris" class="btn btn-light">Cancel</a>
    </form>
  </div>
  <script>
    $(document).ready(function () {
      $("#alert").on("click",function(){
        Swal.fire('Berhasil Submit')
      })
  })
  </script>

@endsection
