<div class="theme-setting-wrapper">
    <div id="settings-trigger"><i class="typcn typcn-cog-outline"></i></div>
    <div id="theme-settings" class="settings-panel">
      <i class="settings-close typcn typcn-delete-outline"></i>
      <p class="settings-heading">SIDEBAR SKINS</p>
      <div class="sidebar-bg-options" id="sidebar-light-theme">
        <div class="img-ss rounded-circle bg-light border mr-3"></div>
        Light
      </div>
      <div class="sidebar-bg-options selected" id="sidebar-dark-theme">
        <div class="img-ss rounded-circle bg-dark border mr-3"></div>
        Dark
      </div>
      <p class="settings-heading mt-2">HEADER SKINS</p>
      <div class="color-tiles mx-0 px-4">
        <div class="tiles success"></div>
        <div class="tiles warning"></div>
        <div class="tiles danger"></div>
        <div class="tiles primary"></div>
        <div class="tiles info"></div>
        <div class="tiles dark"></div>
        <div class="tiles default border"></div>
      </div>
    </div>
  </div>

  <nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
      <li class="nav-item">
        <div class="d-flex sidebar-profile">
          <div class="sidebar-profile-image">
            <img src="{{asset('/template/images/faces/face29.png')}}" alt="image">
            <span class="sidebar-status-indicator"></span>
          </div>
          <div class="sidebar-profile-name">
            <p class="sidebar-name">
              {{Auth::user()->nama}}
            </p>
            <p class="sidebar-designation">
              Welcome
            </p>
          </div>
        </div>
        <p class="sidebar-menu-title">Dash menu</p>
      </li>
      @if(Auth::user()->role == 1 )
      <li class="nav-item">
        <a class="nav-link" href="/dashboard">
          <i class="typcn typcn-device-desktop menu-icon"></i>
          <span class="menu-title">Dashboard</span>
        </a>
      </li>

        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/kategoris">
          <i class="typcn typcn-th-small menu-icon"></i>
          <span class="menu-title">Kategori </span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/mobil">
          <i class="typcn typcn-folder-add menu-icon"></i>
          <span class="menu-title">Mobil </span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
          <i class="typcn typcn-shopping-cart menu-icon"></i>
          <span class="menu-title">Sewa</span>
          <i class="typcn typcn-chevron-right menu-arrow"></i>
        </a>
        <div class="collapse" id="ui-basic">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="/sewa">Form Sewa</a></li>
            <li class="nav-item"> <a class="nav-link" href="/detail">Detail Sewa</a></li>
            {{-- <li class="nav-item"> <a class="nav-link" href="pages/ui-features/dropdowns.html">Form Kembali</a></li> --}}
          </ul>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/users">
          <i class="typcn typcn-th-small menu-icon"></i>
          <span class="menu-title">Pengguna </span>
        </a>
      </li>
      @else
      <li class="nav-item">
        <a class="nav-link" href="/member/sewa">
          <i class="typcn typcn-th-small menu-icon"></i>
          <span class="menu-title">Form Sewa </span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/member/detail">
          <i class="typcn typcn-th-small menu-icon"></i>
          <span class="menu-title">Detail Sewa </span>
        </a>
      </li>
      {{-- <li class="nav-item">
        <a class="nav-link" href="">
          <i class="typcn typcn-th-list-outline menu-icon"></i>
          <span class="menu-title">Form Kembali </span>
        </a>
      </li> --}}
      @endif
    </ul>
    </nav>