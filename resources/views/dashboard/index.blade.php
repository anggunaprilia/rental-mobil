@extends('Layout.master')

@section('judul')
    Dashboard
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="card border-primary my-5" style="width: 18rem; height: 13rem;">
                <div class="card-header"></div>
                <div class="card-body text-primary">
                    <h5 class="card-title">Mobil</h5>
                    <h6 class="card-subtitle mb-2 text-muted"> Jumlah Mobil</h6>
                    <p class="card-text">{{$hitungmobil}}</p>
                  </div>
              </div>

              <div class="card border-primary my-5" style="width: 18rem; height: 13rem; margin-left:auto;">
                <div class="card-header"></div>
                <div class="card-body text-primary">
                    <h5 class="card-title">Kategori</h5>
                    <h6 class="card-subtitle mb-2 text-muted"> Jumlah Kategori</h6>
                    <p class="card-text">{{$hitungkategori}}</p>
                  </div>
              </div>

              <div class="card border-primary my-5" style="width: 18rem; height: 13rem; margin-left:auto;">
                <div class="card-header"></div>
                <div class="card-body text-primary">
                    <h5 class="card-title">Pengguna</h5>
                    <h6 class="card-subtitle mb-2 text-muted"> Jumlah Pengguna</h6>
                    <p class="card-text">{{$hitunguser}}</p>
                  </div>
              </div>
        </div>
      </div>

@endsection