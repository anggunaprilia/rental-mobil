@extends('Layout.master')

@section('judul')
 Edit Data Profil
@endsection

@section('content')

<form action= "/profil/{{$profil->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label>Nama</label>
      <input type="text" name="nama" value="{{$profil->nama}}" class="form-control">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
      <label>Bio</label>
      <input type="text" name="bio" value="{{$profil->bio}}" class="form-control" id="exampleInputPassword1">
    </div>
    @error('bio')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
      <label>Alamat</label>
      <textarea name="alamat" class="form-control" id="" cols="30" rows="10">{{$profil->alamat}}</textarea>
    </div>
    @error('alamat')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
    <script>
      const btn = document.getElementClassName('btn btn-primary mr-2');
      btn.addEventListener('click', function(){
        Swal.fire('Berhasil Submit')
      });
    </script>
  </form>

  @endsection