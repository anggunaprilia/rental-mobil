@extends('Layout.master')

@section('table')
 Detail Data Profil
@endsection

@section('content')


<h1>{{$profil->nama}}</h1>
<p>{{$profil->bio}}</p>
<p>{{$profil->alamat}}</p>

<a href="/profil" class="btn btn-secondary btm sm">Kembali</a>

@endsection