@extends('Layout.master')

@section('judul')
 Daftar Data Pemain Film Baru
@endsection

@section('content')

<a href="/profil/create" class="btn btn-primary btn-sm mb-3">Tambah</a>
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Lainnya</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($profil as $key => $value)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$value->nama}}</td>
            <td>
                <form action="/profil/{{$value->id}}" method="POST">
    
                    @csrf
                    @method('DELETE')
                    <a href="/profil/{{$value->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/profil/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
          
            </td>
            
        </tr>
        @empty
        <tr>
            <td>Tidak ada data</td>
        </tr>  
        @endforelse
    </tbody>
  </table>
@endsection
