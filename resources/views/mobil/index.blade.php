@extends('Layout.master')

@section('judul')
    Data Mobil
@endsection

@section('content')

@push('scripts')
{{-- <script src="{{asset('/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script> --}}
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
<script src="https://cdn.datatables.net/v/bs4/dt-1.13.5/datatables.min.js"></script>
@endpush

@push('style')
<link href="https://cdn.datatables.net/v/bs4/dt-1.13.5/datatables.min.css" rel="stylesheet"/>
@endpush


<div class="card-body">
    <h4 class="card-title">Data Mobil</h4>
    <a href="/mobil/create" class="btn btn-secondary btn-rounded btn-fw">Tambah</a>
    <a href="/pdf" type="button" class="btn btn-outline-info btn-icon-text btn-rounded">
      Print PDF
      <i class="typcn typcn-printer btn-icon-append"></i>
    </a>
    <div class="table-responsive my-3" >
      <table id="example1" class="table table-striped">
        <thead>
          <tr>
            <th>No</th>
            <th>
              Nama
            </th>
            <th>
              Harga
            </th>
            <th>
              Jumlah Kursi
            </th>
            <th>
              Kategori
            </th>
            <th>
                Gambar
            </th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($mobil as $item)
              <tr>
                <td scope="row">{{$loop -> iteration}}</td>
                <td>{{$item -> nama}}</td>
                <td>{{$item -> harga}}</td>
                <td>{{$item -> jumlah_kursi}}</td>
                <td>{{$item -> kategori_id}}</td>
                <td>
                  <img src="{{asset('img/'.$item->gambar)}}" style="width: 50px; height:50px; border-radius: 0px" alt="">
                </td>
                <td>

                  <a href="/mobil/{{$item->id}}/edit" type="submit" class="btn btn-warning btn-rounded btn-fw " style="width: 90px;">Edit</a>
                        {{-- //<a href="/mobil/{{$item->id}}" type="submit" class="btn btn-danger btn-rounded btn-fw">Hapus</a> --}}
                          <form action="/mobil/{{$item->id}}" method="POST" class="my-2" >
                            @csrf
                            @method('delete')
                            <input type="submit" class="btn btn-danger btn-rounded btn-fw" id="alert" style="width: 90px;" value="delete">
                          </form>
                          <script>
                            $(document).ready(function () {
                              $("#alert").on("click",function(){
                                Swal.fire('Berhasil Submit')
                              })
                          })
                          </script>

                </td>
              </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
@endsection