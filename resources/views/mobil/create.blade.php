@extends('Layout.master')

@section('judul')
    Tambah Mobil
@endsection

@section('content')
<div class="card-body">
    <h4 class="card-title">Tambah Data Mobil</h4>
    <form action="/mobil" class="forms-sample" method="POST" enctype="multipart/form-data">
        @csrf
      <div class="form-group">
        <label for="exampleInputName1">Nama</label>
        <input type="text" class="form-control" name="nama" id="nama" >
      </div>
      <div class="form-group">
        <label for="exampleInputEmail3">Harga</label>
        <input type="text" class="form-control" name="harga" id="harga">
      </div>
      <div class="form-group">
        <label for="exampleInputPassword4">Jumlah Kursi</label>
        <input type="text" class="form-control" name="jumlah_kursi" id="jumlah_kursi" >
      </div>
      <div class="form-group">
        <label for="exampleFormControlSelect2">Default select</label>
        <select class="form-control" name="kategori_id" id="exampleFormControlSelect2">
            <option value="">Pilih Kategori</option>
          @forelse ($kategori as $item)
              <option value="{{$item->id}}">{{$item->jenis}}</option>
          @empty
              <option value="">Tidak ada data</option>
          @endforelse
        </select>
      </div>
      <div class="form-group">
        <label>File upload</label>
        <div class="input-group col-xs-12">
          <input type="file" class="form-control file-upload-info" name="gambar" id="gambar"  >
        </div>
      </div>
      <button type="submit" class="btn btn-primary mr-2" id="alert">Submit</button>
      <a href="/mobil" class="btn btn-light">Cancel</a>
    </form>
  </div>
 <script>
    $(document).ready(function () {
      $("#alert").on("click",function(){
        Swal.fire('Berhasil Submit')
      })
  })
  </script>

@endsection