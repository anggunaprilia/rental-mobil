@extends('Layout.master')

@section('judul')
    Edit Mobil
@endsection

@section('content')
<div class="card-body">
    <h4 class="card-title">Edit Data Mobil</h4>
    <form action="/mobil/{{$mobil->id}}" class="forms-sample" method="POST" enctype="multipart/form-data">
        @csrf
        @method('put')
      <div class="form-group">
        <label for="exampleInputName1">Nama</label>
        <input type="text" class="form-control" name="nama" id="nama" value="{{$mobil->nama}}" >
      </div>
      <div class="form-group">
        <label for="exampleInputEmail3">Harga</label>
        <input type="text" class="form-control" name="harga" id="harga" value="{{$mobil->harga}}" >
      </div>
      <div class="form-group">
        <label for="exampleInputPassword4">Jumlah Kursi</label>
        <input type="text" class="form-control" name="jumlah_kursi" id="jumlah_kursi"  value="{{$mobil->jumlah_kursi}} ">
      </div>
      <div class="form-group">
        <label for="exampleFormControlSelect2">kategori</label>
        <select class="form-control" name="kategori_id" value="{{ $mobil->kategori_id }}" id="exampleFormControlSelect2">
            <option  disabled>{{ $mobil->kategori_id }}</option>
            @forelse ($kategori as $item)
              <option value="{{$item->id}}">{{$item->jenis}}</option>
          @empty
              <option value="">Tidak ada data</option>
          @endforelse
        </select>
      </div>
      <div class="form-group">
        <label>File upload</label>
        <div class="input-group col-xs-12">
          <input type="file" class="form-control file-upload-info" name="gambar" id="gambar"  placeholder="Upload Image">
        </div>
        <div class="my-3">
            <img src="{{asset('img/'.$mobil->gambar)}}" alt="" style="width: 50px;">
          </div>
      </div>
      <button type="submit" class="btn btn-primary mr-2" id="alert">Submit</button>
      <a href="/mobil" class="btn btn-light">Cancel</a>
    </form>
  </div>
  <script>
    $(document).ready(function () {
      $("#alert").on("click",function(){
        Swal.fire('Berhasil Submit')
      })
  })
  </script>


@endsection