@extends('Layout.master')

@section('judul')

@endsection

@section('content')
    <div class="container">
        <div class="row">
        @foreach($mobil as $item)
          <div class="col">
            <div class="card my-5" style="width: 18rem;" >
                <img src="{{asset ('img/'.$item->gambar)}}" class="card-img-top" style="height: 200px">
                <div class="card-body">
                    <h5 class="card-title">{{$item->nama}}</h5>
                    <p class="card-text">Harga sewa perhari Rp. {{$item->harga}}</p>
                    <p class="card-text">Jumlah Kursi {{$item->jumlah_kursi}}</p>
                  <a href="/member/sewa" class="btn btn-primary" id="alert">Sewa</a>
                </div>
              </div>
          </div>
          <script>
            $(document).ready(function () {
              $("#alert").on("click",function(){
                Swal.fire('Berhasil Sewa')
              })
          })
          </script>
        @endforeach
        </div>
      </div>



@endsection