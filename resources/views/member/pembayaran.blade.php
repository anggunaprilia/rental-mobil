

@extends('Layout.master')

@section('judul')
    pembayaran
@endsection

@section('content')
<div class="card-body">
    <h4 class="card-title">pembayaran</h4>
    <form action="/member/detail/{{$sewa->id}}" class="forms-sample" method="POST" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Upload pembayaran</label>
            <div class="input-group col-xs-12">
              <input type="file" class="form-control file-upload-info" name="pembayaran" id="pembayaran"  placeholder="Upload Image">
            </div>
            {{-- <div class="my-3">
                <img src="{{asset('img/'.$mobil->gambar)}}" alt="" style="width: 50px;">
              </div> --}}
          </div>
      <button type="submit" class="btn btn-primary mr-2">Submit</button>
      <a href="/detail" class="btn btn-light" id="alert">Cancel</a>
    </form>
  </div>
  <script>
    $(document).ready(function () {
      $("#alert").on("click",function(){
        Swal.fire('Berhasil Sewa')
      })
  })
  </script>


@endsection