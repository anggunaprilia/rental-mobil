

@extends('Layout.master')

@section('judul')
    Form sewa mobil
@endsection

@section('content')
<div class="card-body">
    <h4 class="card-title">form sewa</h4>
    <form action="/member/sewaproses" class="forms-sample" method="POST">
        @csrf
        <div class="form-group">
            <label for="exampleFormControlSelect2">Penyewa</label>
            <select class="form-control" name="user_id" id="exampleFormControlSelect2">
                <option value="">Pilih Penyewa</option>
                @forelse ($user as $item)
                    <option value="{{$item->id}}">{{$item->nama}}</option>
                @empty
                    <option value="">Tidak ada data</option>
                @endforelse
            </select>
        </div>
        <div class="form-group">
            <label for="exampleFormControlSelect2">Mobil</label>
            <select class="form-control" name="mobil_id" id="exampleFormControlSelect2">
                <option value="">Pilih Mobil</option>
                @forelse ($mobil as $item)
                    <option value="{{$item->id}}">{{$item->nama}}</option>
                @empty
                    <option value="">Tidak ada data</option>
                @endforelse
            </select>
        </div>
        <div class="form-group">
            <label for="exampleFormControlSelect2">Hari</label>
            <select class="form-control" name="durasi_sewa" id="exampleFormControlSelect2">
                <option value="1">1 hari</option>
                <option value="2">2 hari</option>
                <option value="3">3 hari</option>
                <option disabled>lebih dari 3 hari silahkan datang ke store</option>
            </select>
        </div>
      <button type="submit" class="btn btn-primary mr-2">Submit</button>
      <a href="/member" class="btn btn-light">Cancel</a>
    </form>
  </div>
  <script>
    $(document).ready(function () {
      $("#alert").on("click",function(){
        Swal.fire('Berhasil Submit')
      })
  })
  </script>


@endsection
