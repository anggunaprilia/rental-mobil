@extends('Layout.master')

@section('judul')
    Detail Sewa
@endsection

@section('content')

<div class="card-body">
    <h4 class="card-title">Detail Sewa</h4>
    @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
    <div class="table-responsive my-3">
      <x-detail :sewa='$sewa'/>
    </div>
  </div>
@endsection