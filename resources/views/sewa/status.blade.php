

@extends('Layout.master')

@section('judul')
    status pembayaran
@endsection

@section('content')
<div class="card-body">
    <h4 class="card-title">status pembayaran</h4>
    <form action="/status/{{$sewa->id}}" class="forms-sample" method="POST" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="form-group">
            <div class="my-3">
                <img src="{{asset('img/'.$sewa->pembayaran)}}" alt="" style="width: 300px;">
              </div>
          </div>
        <div class="form-group">
            <label for="exampleFormControlSelect2">Status</label>
            <select class="form-control" name="status_pembayaran" id="exampleFormControlSelect2">
                <option value="diterima">Diterima</option>
                <option value="ditolak">Ditolak</option>
            </select>
        </div>
      <button type="submit" class="btn btn-primary mr-2">Submit</button>
      <a href="/detail" class="btn btn-light">Cancel</a>
      <script>
        const btn = document.getElementByClassName('btn btn-primary mr-2');
        btn.addEventListener('click', function(){
          Swal.fire('Berhasil Submit')
        });
      </script>
    </form>
  </div>


@endsection