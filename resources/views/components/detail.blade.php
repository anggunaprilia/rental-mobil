<div>
    <!-- Do what you can, with what you have, where you are. - Theodore Roosevelt -->
    {{-- {{$sewa}} --}}
    <table id="example1" class="table table-striped">
        <thead>
          <tr>
            <th>No</th>
            <th>
              Penyewa
            </th>
            <th>Mobil</th>
            <th>durasi sewa</th>
            <th>jumlah</th>
            <th>pembayaran</th>
            <th>status pembayaran</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($sewa as $item)
              <tr>
                <td scope="row">{{$loop -> iteration}}</td>
                <td>{{$item -> user_id}}</td>
                <td>{{$item -> Mobil->nama}}</td>
                <td>{{$item -> durasi_sewa}}</td>
                <td>
                    {{$total = ($item -> Mobil->harga) * ($item ->durasi_sewa)}}
                </td>
                <td>
                    <img src="{{ $item->pembayaran != null ? asset('img/'.$item->pembayaran) : "none"}}" style="width: 50px; height:50px; border-radius: 0px" alt="">
                  </td>

                <td>{{$item -> status_pembayaran}}</td>
                <td>

                    @if (Auth::user()->role == 1)
                    <a href="/detail/{{$item->id}}" class="btn btn-info btn-sm my-1" style="width: 133px">pembayaran</a>
                    <form action="/user/{{$item->id}}" method="POST">
                        @csrf
                        @method('DELETE')

                        <a href="/detail/{{$item->id}}/status" class="btn btn-warning btn-sm">status</a>
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                    <script>
                      const btn = document.getElementClassName('btn btn-primary mr-2');
                      btn.addEventListener('click', function(){
                        Swal.fire('Berhasil Submit')
                      });
                    </script>
                    </form>
                    @elseif(Auth::user()->role == 2)
                    <a href="/member/detail/{{$item->id}}" class="btn btn-info btn-sm my-1" style="width: 133px">pembayaran</a>
                    @endif

                </td>
              </tr>
          @endforeach
        </tbody>
      </table>
</div>